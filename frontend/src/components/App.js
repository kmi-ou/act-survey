import React from "react";
import {BrowserRouter, Route, Switch } from  "react-router-dom";

import ThankYouPage from "./ThankYouPage";
import NotFoundPage from "./NotFoundPage";
import SurveyPage from "./SurveyPage";

import "../style/App.scss";


export default () => {
    return (
        <BrowserRouter>
            <Switch>
                <Route path="/survey/:token" component={SurveyPage} exact />
                <Route path="/thank-you" component={ThankYouPage} exact />
                <Route component={NotFoundPage} exact />
            </Switch>
        </BrowserRouter>
    );
}
