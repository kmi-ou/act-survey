import React, { Component } from "react";
import pdfjs from "pdfjs-dist/webpack";

import Spinner from "./Spinner";


class PdfLoader extends Component {
    constructor(props) {
        super(props);
        this.state = {
            pdfDocument: null
        };
    }

    componentDidMount() {
        /**
         * Load PDF when this component is inserted into the DOM
         */
        const { url } = this.props;

        pdfjs.getDocument(url).then(pdfDocument => {
            this.setState({
                pdfDocument: pdfDocument
            });
        });
    }


    render() {
        const { children } = this.props;
        const { pdfDocument } = this.state;

        if (pdfDocument) {
            return children(pdfDocument);
        }

        return <Spinner/>
    }
}

export default PdfLoader;
