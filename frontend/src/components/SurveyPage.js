import React, { Component } from "react";

import SideMenu from "./SideMenu";
import Survey from "./Survey";
import ReactTooltip from "react-tooltip"
import { Redirect } from "react-router-dom";
import ServerError from "./ServerError";
import axios from "axios";
import Spinner from "./Spinner";
import {BACKEND_SERVER_URL} from "../settings"

export default class SurveyPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showNotFound: false,
            showThankYouPage: false,
            showServerError: false,
            isLoaded: false,
            scrollToFirst: false,
            pdfTitle: null,
            pdfUrl: null,
            citations: [],
            width: null,
            height: null
        };
    }

    componentDidMount() {
        axios
            .get(BACKEND_SERVER_URL + "/backend/get/survey/" + this.props.match.params.token)
            .then(
                (result) => {
                    if(result.status === 204){
                        this.setState({
                            showThankYouPage: true
                        })
                    }
                    else if(result.status === 200){
                        this.setState({
                            isLoaded: true,
                            pdfTitle: result.data.title,
                            pdfUrl: result.data.url,
                            citations: result.data.citations ? [...result.data.citations] : [],
                            width: result.data.width,
                            height: result.data.height
                        });
                    }
                    else{
                        this.setState({
                            showServerError: true
                        })
                    }

                }
            )
            .catch(
                (error) => {
                    if(error.response && error.response.status === 404){
                        this.setState({
                            showNotFound: true
                        })
                    }
                    else{
                        this.setState({
                            showServerError: true
                        });
                    }
                }
            );
    }

    shouldComponentUpdate(nextProps, nextState) {
        if(!this.state.isLoaded && nextState.isLoaded){
            return true;
        }
        if(nextState.showServerError || nextState.showThankYouPage || nextState.showNotFound){
            return true;
        }

        return !this.state.scrollToFirst && nextState.scrollToFirst;
    }

    scrollToFirstCitance(){
        this.setState({
            scrollToFirst: true
        })
    }

    render() {
        if(this.state.showThankYouPage){
            return <Redirect to="/thank-you" />
        }
        else if(this.state.showNotFound){
            return <Redirect to="/404" />
        }

        else if(this.state.showServerError){
            return <ServerError/>
        }
        else if(!this.state.isLoaded){

            return <Spinner/>
        }
        else {
            return (
                <div className="App">
                    <ReactTooltip/>
                    <SideMenu
                        scrollToFirstCitance={() => {
                            this.scrollToFirstCitance();
                        }}
                        pdfTitle={this.state.pdfTitle}
                    />
                    <Survey
                        scrollToFirst={this.state.scrollToFirst}
                        citation={this.state.citations.length ? this.state.citations[0] : null}
                        citations={this.state.citations}
                        pdfUrl={this.state.pdfUrl}
                        width={this.state.width}
                        height={this.state.height}
                        token={this.props.match.params.token}
                    />
                </div>
            );
        }
    }
}
