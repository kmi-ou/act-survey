import React, { Component } from "react";
import LinearProgress from '@material-ui/core/LinearProgress';

import "../style/QuestionForm.scss";

export default class QuestionForm extends Component {
    constructor(props) {
        super();
        this.state = {
            completed: props.completed,
            id: props.citationId,
            firstQuestion: null,
            firstQuestionStyle: {},
            comComFirstQuestion: null,
            secondQuestion: null,
            secondQuestionStyle: {},
            submitValue: "Next citation"
        };

        this.handleChangeOne = this.handleChangeOne.bind(this);
        this.handleChangeTwo = this.handleChangeTwo.bind(this);
        this.handleComComChange = this.handleComComChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.clear === true) {
            this.setState({
                id: nextProps.citationId,
                firstQuestion: null,
                secondQuestion: null,
                comComFirstQuestion: null,
                submitValue: nextProps.submitValue ? "Finish" : "Next"
            });
        }
    }

    progress = () => {
        const { completed } = this.state;
        if (completed === 100) {
            this.setState({ completed: 0 });
        } else {
            const diff = Math.random() * 10;
            this.setState({ completed: Math.min(completed + diff, 100) });
        }
    };

    handleSubmit = e => {
        e.preventDefault();
        if (!this.state.firstQuestion) {
            this.setState({
                firstQuestionStyle: {
                    color: "red"
                }
            });
        } else {
            this.setState({ firstQuestionStyle: {} });
        }

        if (!this.state.secondQuestion) {
            this.setState({
                secondQuestionStyle: {
                    color: "red"
                }
            });
        } else {
            this.setState({ secondQuestionStyle: {} });
        }
        if (this.state.firstQuestion && this.state.secondQuestion) {
            this.props.onSubmit({
                id: this.state.id,
                firstQuestion: this.state.firstQuestion,
                secondQuestion: this.state.secondQuestion,
                comComFirstQuestion: this.state.comComFirstQuestion
            });
        }
    };

    handleChangeOne(e) {
        this.setState({
            firstQuestion: e.target.value,
            firstQuestionStyle: {}
        });
    }

    handleComComChange(e) {
        this.setState({
            comComFirstQuestion: e.target.value,
        });
    }

    handleChangeTwo(e) {
        this.setState({
            secondQuestion: e.target.value,
            secondQuestionStyle: {}
        });
    }


    render() {
        return (
            <form className="survey">
                <div>
                    <h6 style={this.state.firstQuestionStyle}>
                        How would you best describe your reason for citing this paper in your work?
                        {this.state.firstQuestionStyle.color ? " *Required" : null}
                    </h6>
                    <div className="form-group">
                        <label className="form-radio">
                            <input
                                type="radio"
                                name="background"
                                value="background"
                                checked={this.state.firstQuestion === "background"}
                                onChange={this.handleChangeOne}
                            />
                            <i className="form-icon"/>
                            <span
                                data-tip="
                                    The paper you are citing provides relevant information
                                    or is part of the body of literature in this domain.
                                    "
                                data-multiline
                            >
                                Background
                            </span>

                        </label>
                        <label className="form-radio">
                            <input
                                type="radio"
                                name="uses"
                                value="uses"
                                checked={this.state.firstQuestion === "uses"}
                                onChange={this.handleChangeOne}
                            />
                            <i className="form-icon"/>
                            <span
                                data-tip="
                                    Your paper uses the methodology or tools created
                                    by the paper you are citing.
                                    "
                                data-multiline
                            >
                                Uses
                            </span>
                        </label>
                        <label className="form-radio">
                            <div className="accordion">
                                <input
                                    type="radio"
                                    name="compares/contrasts"
                                    value="compares_contrasts"
                                    checked={this.state.firstQuestion === "compares_contrasts"}
                                    onChange={this.handleChangeOne}
                                />
                                <i className="form-icon"/>
                                <span
                                    data-tip="
                                        Your paper expresses differences / similarities
                                        to the paper you are citing.
                                        "
                                    data-multiline
                                >
                                    Compares/Contrasts
                                </span>
                                <div className="accordion-body" style={{ marginBottom: "0" }}>
                                    <label className="form-radio">
                                        <input
                                            type="radio"
                                            name="similarities"
                                            value="similarities"
                                            checked={this.state.comComFirstQuestion === "similarities"}
                                            onChange={this.handleComComChange}
                                        /> Pointing out similarities with cited paper
                                        <i className="form-icon"/>
                                    </label>
                                    <label className="form-radio">
                                        <input
                                            type="radio"
                                            name="differences"
                                            value="differences"
                                            checked={this.state.comComFirstQuestion === "differences"}
                                            onChange={this.handleComComChange}
                                        /> Pointing out differences with cited paper
                                        <i className="form-icon"/>
                                    </label>
                                    <label className="form-radio">
                                        <input
                                            type="radio"
                                            name="refuting"
                                            value="refuting"
                                            checked={this.state.comComFirstQuestion === "refuting"}
                                            onChange={this.handleComComChange}
                                        /> Disagree with cited Paper
                                        <i className="form-icon"/>
                                    </label>
                                </div>
                            </div>
                        </label>
                        <label className="form-radio">
                            <input
                                type="radio"
                                name="motivation"
                                value="motivation"
                                checked={this.state.firstQuestion === "motivation"}
                                onChange={this.handleChangeOne}
                            />
                            <i className="form-icon"/>
                            <span
                                data-tip="
                                        Your paper is directly motivated by the paper you are citing.
                                    "
                                data-multiline
                            >

                                Motivation
                            </span>
                        </label>
                        <label className="form-radio">
                            <input
                                type="radio"
                                name="extension"
                                value="extension"
                                checked={this.state.firstQuestion === "extension"}
                                onChange={this.handleChangeOne}
                            />
                            <i className="form-icon"/>
                            <span
                                data-tip="
                                    Your paper extends the data, methods etc. of the paper
                                    you are citing.
                                    "
                                data-multiline
                            >
                                Extension
                            </span>
                        </label>
                        <label className="form-radio">
                            <input
                                type="radio"
                                name="future"
                                value="future"
                                checked={this.state.firstQuestion === "future"}
                                onChange={this.handleChangeOne}
                            />
                            <i className="form-icon"/>
                            <span
                                data-tip="
                                    The paper you are citing is potential
                                    avenue for future work.
                                    "
                                data-multiline
                            >
                                Future
                            </span>
                        </label>
                    </div>
                </div>
                <div>
                    <h6 style={this.state.secondQuestionStyle}>
                        Would you describe this citation as central (influential) to your paper
                        or was it peripheral (incidental)?
                        {this.state.secondQuestionStyle.color ? " *Required" : null}
                    </h6>
                    <div className="form-group">
                        <label className="form-radio">
                            <input
                                type="radio"
                                name="incidental"
                                value="incidental"
                                checked={this.state.secondQuestion === "incidental"}
                                onChange={this.handleChangeTwo}
                            />
                            <i className="form-icon"/> Incidental
                        </label>
                        <label className="form-radio">
                            <input
                                type="radio"
                                name="influential"
                                value="influential"
                                checked={this.state.secondQuestion === "influential"}
                                onChange={this.handleChangeTwo}
                            />
                            <i className="form-icon"/> Influential
                        </label>
                    </div>
                </div>
                <LinearProgress
                    color="primary"
                    className="act-progress-bar"
                    variant="determinate"
                    value={this.props.completed}
                />

                <div style={{minHeight: "40px"}}>
                    <input
                        type="submit"
                        className="btn btn-primary"
                        onClick={(e) => this.handleSubmit(e)}
                        value={this.state.submitValue}
                        style={{float: "right"}}
                    />
                </div>
            </form>
        );
    }
}
