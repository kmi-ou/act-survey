import React, { Component } from "react";
import { slide as Menu } from "react-burger-menu";

import "../style/SideMenu.scss";
import helpImg from "../../img/help.png";


export default class SideMenu extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showButton: true,
            isOpen: true,
            scrolled: false
        };
    }

    shouldComponentUpdate(nextProps, nextState) {
        if(!nextState.isOpen && !this.state.scrolled){
            this.props.scrollToFirstCitance();
        }

        return !nextState.showButton && this.state.showButton;
    }


    render () {
        return (
            <Menu
                customBurgerIcon={ <img src={helpImg} /> }
                isOpen={ this.state.isOpen }
                onStateChange={ (state) => {
                    this.setState({
                        isOpen: state.isOpen
                    });
                }}
                width={ "50vw" }
                left
            >
                <h3><b>A</b>cademic <b>C</b>itation <b>T</b>yping Survey</h3>
                   <p>
                       Thank you for taking part in the Academic Citation Typing (ACT) Survey.
                       Displayed on the right of the page is your paper entitled:
                   </p>
                <blockquote>
                    <h4><i>
                        {this.props.pdfTitle}
                    </i></h4>
                </blockquote>
                <p>
                    You are asked to assign a classification for the citations
                    in your paper from the following categories:
                </p>

                <p>
                    <b>BACKGROUND</b> - The paper you are citing provides relevant information or is part of the body of literature in this domain.
                    <br/>
                    <b>USES</b> - Your paper uses the methodology or tools created by the paper you are citing.
                    <br/>
                    <b>COMPARES/CONTRASTS</b> - Your paper expresses differences / similarities to the paper you are citing.
                    <br/>
                    <b>MOTIVATED BY</b> - Your paper is directly motivated by the paper you are citing.
                    <br/>
                    <b>EXTENSION</b> - Your paper extends the data, methods etc. of the paper you are citing.
                    <br/>
                    <b>FUTURE</b> - The paper you are citing is potential avenue for future work.
                </p>

                <p>
                    The marker for the current citation to be classified will be highlighted in the PDF
                    on the right of the page. Shown on the left of the page is the sentence containing the citation.
                </p>

                <p>
                    Please select the closest classification type for each citation using the checkboxes.
                </p>

                <p>
                    We'd also like to know whether you feel this citation was central to your paper (influential),
                    or was it more peripheral (incidental)
                </p>

                <p>
                    Clicking ‘Next Citation’ will display the next citation to be annotated.
                </p>

                <p className="small-text">
                    Please note: The survey platform will remember your choices each time ‘next’ is clicked.
                    You do not need to classify every citation in your paper. The more you are able to do,
                    of course the happier we are! You can also leave this page it will return to the same point
                    in the survey. Thank you!
                </p>

                {
                    this.state.showButton ? <div style={{ textAlign: "center"}}>
                    <button
                        className="btn"
                        onClick={
                            () => this.setState(
                                {isOpen: false, showButton: false}
                                )
                        }
                    >
                        Let's go
                    </button>
                    </div> : ""
                }
            </Menu>
        );
    }
}
