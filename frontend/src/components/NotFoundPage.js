import React  from "react";

export default () => {
    return (
        <div className="container">
            <div className="columns">
                <div className="column col-12" style={{textAlign: "center", paddingTop: "10rem"}}>
                    <h1>Page Not Found</h1>
                </div>
            </div>
        </div>
    )
}
