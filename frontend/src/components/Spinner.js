import React from "react";

export default () => {
    return (
        <div
            style={{
                display: "flex",
                alignItems: "center",
                height: "100vh",
                justifyContent: "center"
            }}
        >
            <div className="loading loading-lg" />
        </div>
    );
};
