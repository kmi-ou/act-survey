import React, {Component} from "react";
import  ScrollArea from "react-scrollbar";
import ReactDom from "react-dom";

import "../style/Sidebar.scss";


class Sidebar extends Component {
    componentDidUpdate(){
        ReactDom.findDOMNode(this).parentNode.scrollIntoView();
    }

    render() {
        return (
            <div className="metadata">
                <h6 style={{ marginBottom: "1rem" }}>
                    Highlighted Citation Marker From:
                </h6>
                <table className="table">
                    <tbody>
                    <tr className="">
                        <td>Title</td>
                        <td><b>{this.props.title}</b></td>
                    </tr>
                    <tr className="">
                        <td>Year</td>
                        <td><b>{this.props.year}</b></td>
                    </tr>
                    <tr className="">
                        <td>Author</td>
                        <td><b>{this.props.author}</b></td>
                    </tr>
                    </tbody>
                </table>
                <h6 style={{ paddingTop: "10px" }}>Sentence containing citation:</h6>
                <ScrollArea
                    speed={0.8}
                    className="area"
                    contentClassName="content"
                    horizontal={false}
                    style={{ maxHeight: "100px" }}
                >
                    <blockquote>
                        <p>{this.props.citation.citance}</p>
                    </blockquote>
                </ScrollArea>
            </div>
        );
    }
}

export default Sidebar;
