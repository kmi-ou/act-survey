import React, { Component } from "react";
import { Redirect } from  "react-router-dom";

import PdfLoader from "./PdfLoader";
import PdfHighlighter from "./PdfHighlighter";
import QuestionForm from "./QuestionForm";
import Sidebar from "./Sidebar";
import {BACKEND_SERVER_URL} from "../settings";
import axios from "axios";

export default class Survey extends Component {
    values = [];

    constructor(props) {
        super(props);
        this.state = {
            index: 0,
            url: props.pdfUrl,
            citation: props.citation,
            citations: props.citations,
            title: props.citations.title,
            clear: false,
            finish: false,
            width: props.width,
            height: props.height,
            scrollToFirst: props.scrollToFirst
        };
    }
    componentWillReceiveProps(nextProps, nextContext) {
        if(!this.props.scrollToFirst && nextProps.scrollToFirst){
            this.setState({
                scrollToFirst: true
            })
        }
    }

    getCompleted() {
        if(!this.state.citations.length){
            return 0
        }
        return (this.state.index + 1) * 100 / this.state.citations.length;
    }

    /**
     * This function will be replaced from PdfHighlighter component
     */
    scrollViewerTo = (citation) => {
    };

    onSubmit = (fields) => {
        axios.post(BACKEND_SERVER_URL + "/backend/store/survey/" + this.props.token, fields)
            .then(function (response) {
            })
            .catch(function (error) {
            });

        if(this.state.citations.length === this.state.index + 1){
            this.setState({
                finish: true
            });
        }
        else {
            this.setState({
                citation: this.state.citations[this.state.index + 1],
                index: this.state.index + 1,
                clear: true
            });
        }
    };

    componentDidUpdate(){
        this.scrollViewerTo(this.state.citation);
    }

    render() {
        if(this.state.finish){
            return <Redirect to="/thank-you" />
        }
        return (
            <div style={{ display: "flex", height: "100vh" }}>
                <div className="bg-light sidebar-outer" style={{ width: "45vw", padding: "1rem", height: "100%"}}>
                    <div style={{ margin: ".5rem", height: "auto" }}>
                        <Sidebar
                            citation={this.state.citation}
                            title={this.state.citation.title}
                            author={this.state.citation.author}
                            year={this.state.citation.pub_year}
                        />
                        <QuestionForm
                            submitValue={this.state.citations.length === this.state.index + 1}
                            citationId={this.state.citation.id}
                            onSubmit={(fields) => this.onSubmit(fields)}
                            clear={this.state.clear}
                            completed={this.getCompleted()}
                        />
                    </div>
                    <div className="partners" style={{ height: "60px" }}>
                        <img style={{ width: "85px", height: "auto", float: "right"}} alt="core logo" src="https://core.ac.uk/recommender/logo.png"/>
                    </div>
                </div>
                <div style={{ height: "100vh", width: "55vw", position: "relative", background: "#eeecec" }}>
                    <PdfLoader url={this.state.url}>
                        {pdfDocument => (
                            <PdfHighlighter
                                scrollToFirst={this.state.scrollToFirst}
                                width={this.state.width}
                                height={this.state.height}
                                pdfDocument={pdfDocument}
                                scrollRef={scrollTo => {
                                    this.scrollViewerTo = scrollTo;
                                }}
                                uniqueIdOld={(this.state.index - 1) + "-act-survey-highlight"}
                                citation={this.state.citation}
                                uniqueId={this.state.index + "-act-survey-highlight"}
                                highlight={this.state.citation}
                            />
                        )}
                    </PdfLoader>
                </div>
            </div>
        );
    }
}
