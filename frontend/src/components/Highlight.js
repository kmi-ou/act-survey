import React  from "react";
import Tooltip from "@material-ui/core/Tooltip";

import "../style/Highlight.scss";


export default class Highlight extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            show: true
        }
    }

    formatPopUp(){
        return <div>
           <b>Author:</b> {this.props.citation.author}
           <br/>
           <b>Title</b> {this.props.citation.title}
           <br />
           <b>Year: </b> {this.props.citation.pub_year}
        </div>
    }

    render() {
        if(!this.props.show){
            return
        }

        const { position, uniqueId } = this.props;

        return (
            <div id={uniqueId} className="act-highlight" style={this.props.style}>
                <Tooltip
                    title={this.formatPopUp()}
                    placement="top"

                >
                    <div
                        style={position}
                        className="act-highlight-part"
                    />
                </Tooltip>
            </div>
        );
    };
}