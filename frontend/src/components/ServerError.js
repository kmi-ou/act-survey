import React  from "react";

export default () => {
    return (
        <div className="container">
            <div className="columns">
                <div className="column col-12" style={{textAlign: "center", paddingTop: "10rem"}}>
                    <h1>We are sorry. Something unexpected happened. Please try it later...</h1>
                </div>
            </div>
        </div>
    )
}
