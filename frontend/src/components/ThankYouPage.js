import React  from "react";

export default () => {
    return (
        <div className="container">
            <div className="columns">
                <div className="column col-6 col-mx-auto">
                    <div style={{textAlign: "center", paddingTop: "1rem"}}>
                        <h1>Thank you for participating in the survey!</h1>
                    </div>
                    <div style={{textAlign: "left"}}>
                        <p>
                            Thank you for participating in the ACT Survey.
                        </p>
                        <p>
                            Your contribution is very much appreciated.
                        </p>
                        <p>
                            Every response helps to build a dataset of annotated
                            citations far bigger and more accurate than anything
                            ever previously achieved.
                        </p>
                        <p>
                            We truly believe that by understanding
                            why academics cite a particular work we can
                            potentially sharpen the blunt tool that
                            is simple bibliometrics.
                        </p>
                        <p>
                            None of the current and widely adopted metrics;
                            Journal Impact Factor (JIF), h-index, Field-weighted
                            citation impact, SCImago Journal Rank or Scopus SNIP,
                            to name but a few, take into consideration
                            the reason for citation.
                        </p>
                        <p>
                            This new dataset will allow us to gain much deeper
                            insights into the nature of citations and,
                            significantly, not just that a paper was cited,
                            but why it was cited.
                        </p>
                        <p>
                            If you would like to be updated with the results
                            of the survey, please send us an email to david.pride@open.ac.uk
                        </p>
                        <p>
                            Once again thank you.
                        </p>
                        <p style={{textAlign: "right"}}>
                            <b>The ACT Survey Team.</b>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    )
}
