import React, { Component } from "react";
import ReactDom from "react-dom";
import { PDFViewer, PDFLinkService } from "pdfjs-dist/web/pdf_viewer";

import Highlight from "./Highlight";

import "pdfjs-dist/web/pdf_viewer.css";
import "../style/PdfHighlighter.scss";


export default  class PdfHighlighter extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isCollapsed: true,
            range: null
        };
    }
    rendered = null;
    oldRendered = null;
    viewer = null;
    containerNode = null;

    scaledToViewport = (scaled, viewport) => {
        const scaled_width = this.props.width;
        const scaled_height = this.props.height;
        const { width, height } = viewport;
        const x1 = width * scaled.coordinates.x1 / scaled_width;
        const y1 = height * scaled.coordinates.y1 /  scaled_height;

        const x2 = width * scaled.coordinates.x2 / scaled_width;
        const y2 = height * scaled.coordinates.y2 / scaled_height;

        return {
            left: x1,
            top: y1,
            width: x2 - x1,
            height: y2 - y1
        };
    };


    componentWillReceiveProps(nextProps) {
        if (this.props.scrollViewerTo !== nextProps.scrollViewerTo) {
            this.renderHighlight();
        }
    }

    shouldComponentUpdate() {
        return false;
    }

    componentDidMount() {
        const { pdfDocument } = this.props;

        this.linkService = new PDFLinkService();

        this.viewer = new PDFViewer({
            container: this.containerNode,
            enhanceTextSelection: true,
            removePageBorders: false,
            linkService: this.linkService
        });

        this.viewer.setDocument(pdfDocument);
        this.linkService.setDocument(pdfDocument);
        this.linkService.setViewer(this.viewer);

        // debug
        window.PdfViewer = this;

        this.containerNode &&
        this.containerNode.addEventListener("pagesinit", () => {
            this.onDocumentReady();
        });

        this.containerNode &&
        this.containerNode.addEventListener("textlayerrendered", this.onTextLayerRendered);
    }

    componentWillUnmount() {
        this.containerNode &&
        this.containerNode.removeEventListener("textlayerrendered", this.onTextLayerRendered);
    }

    findOrCreateHighlightLayer(page) {
        const textLayer = this.viewer.getPageView(page - 1).textLayer;

        if (!textLayer) {
            return null;
        }

        return this.findOrCreateContainerLayer(textLayer.textLayerDiv, "PdfHighlighter__highlight-layer");
    }

    findOrCreateContainerLayer = (container, className) => {
        let layer = container.querySelector(`.${className}`);
        if (!layer) {
            layer = document.createElement("div");
            layer.className = className;
            container.appendChild(layer);
        }

        return layer;
    };

    scaledPositionToViewport(position, pageNumber, width, height) {

        /**
         * {
         *    fontScale: 1.6666666666666665,
         *    height: 1319.9999999999998,
         *    offsetX: 0,
         *    offsetY: 0,
         *    rotation: 0,
         *    scale: 1.6666666666666665,
         *    transform: (6) [1.6666666666666665, 0, 0, -1.6666666666666665, 0, 1319.9999999999998],
         *    viewBox: (4) [0, 0, 612, 792],
         *    width: 1019.9999999999999
         * }
         */
        const viewport = this.viewer.getPageView(pageNumber - 1).viewport;

        return this.scaledToViewport(position, viewport, width, height);
    }

    renderHighlight(force=false) {
        const { highlight } =  this.props;
        if(this.rendered === highlight && !force){
            return
        }

        // create HighLight layer
        const highlightLayer = this.findOrCreateHighlightLayer(highlight.position.page_number);

        if (highlightLayer) {
            const position = this.scaledPositionToViewport(highlight.position, highlight.position.page_number);
            this.rendered = highlight;
            this.setState({});

            if(this.oldRendered){
                ReactDom.unmountComponentAtNode(this.oldRendered);
            }

            this.oldRendered = highlightLayer;
            this.exampleRef = React.createRef();

            ReactDom.render(
                <Highlight
                    ref={this.exampleRef}
                    position={position}
                    citation= {this.props.citation}
                    uniqueId={this.props.uniqueId}
                    show={true}
                />,
                highlightLayer
            );

         }
        }

    onTextLayerRendered = () => {
        this.renderHighlight(true);
        if(this.props.scrollToFirst) {
            this.scrollTo(this.props.highlight);
        }
    };

    scrollTo = (highlight) => {
        try{
            const { position } = highlight;
            const pageNumber = position.page_number;

            const pageViewport = this.viewer.getPageView(pageNumber - 1).viewport;

            const scrollMargin = 200;
            this.viewer.scrollPageIntoView({
                pageNumber,
                destArray: [
                    null,
                    { name: "XYZ" },
                    ...pageViewport.convertToPdfPoint(
                        0,
                        this.scaledToViewport(position, pageViewport).top -
                        scrollMargin
                    ),
                    0
                ]
            });
            this.renderHighlight()

        }
        catch (e) {
            console.log(e);
        }
    };

    onDocumentReady = () => {
        const { scrollRef } = this.props;
        this.viewer.currentScaleValue = "auto";
        const pageNumber = 1;
        scrollRef(this.scrollTo);

        if(!this.props.scrollToFirst){
            this.viewer.scrollPageIntoView({
                pageNumber
            });
        }
    };

    render() {
        return (
            <div
                ref={node => (this.containerNode = node)}
                className="PdfHighlighter"
                onContextMenu={(e) => e.preventDefault()}
            >
                <div className="pdfViewer"/>
            </div>
        );
    }
}
