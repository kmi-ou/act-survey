# ACT Survey

This is the codebase for frontend and backend for Academic Citation Typing (ACT) Survey. 

## Backend
Backend part is written in python 3. It's locaten id `backend` directory. 

### Instalation
Make sure you have already installed python (>=3.0) and vitualenv. 

```bash
cd backend
mkdir venv
virtualenv -p python3 venv # make virtual enviroment
. venv/bin/activate # activate the virtual enviroment
pip install -r requirements.txt # install all required packages
```

### CLI

```python
python -m actsurvey import data [<citing_papers> <cited_papers>]
python -m actsurvey api [--port=<port>]
python -m actsurvey db-init
python -m actsurvey --help
```

You may want to overwrite some variables in `/backend/actsurvey/settings.py`

## Fronted
Frontend is written React and it used pdf.js for displaying the PDFs.

### Instalation
Make sure you have alrady installed nodejs and npm package manager. 

```bash
cd frontend
npm install
```

```bash
npm start # start backround watching processes - for development purposes only
npm run-script build # create production build
```

You may want to overwrite some variables in `/frontend/src/settings.js`
