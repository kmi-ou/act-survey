# -*- coding: utf-8 -*-

import arrow
import json
from flask import Flask, jsonify, request
from flask_cors import CORS

from ..database.db_model import CitedPaper, CitingPaper, ComComType, CitationType, InfRank
from ..helpers import transactional

app = Flask(__name__)
CORS(app)


@app.route("/backend/get/survey/<string:token>", methods=["GET"])
@transactional
def get_info(db_session, token):
    """
    :type token: str
    """
    citing_paper = db_session.query(CitingPaper).filter(CitingPaper.url_token == token).first()

    if not citing_paper:
        return "", 404

    cited_papers = (
        db_session.query(CitedPaper).filter(
            CitedPaper.completion_date.is_(None),
            CitedPaper.url_token == citing_paper.url_token
        ).order_by(CitedPaper.citance_rank).all()
    )

    if not len(cited_papers):
        return "", 204

    return (
        jsonify(
            {
                "url": "/pdfs/{}_{}_{}.pdf".format(citing_paper.ref_id, citing_paper.mag_id, citing_paper.core_id),
                "title": citing_paper.title,
                "width": citing_paper.page_width,
                "height": citing_paper.page_height,
                "citations": [
                    {"id": i.id, "title": i.title, "author": i.author, "pub_year": i.pub_year, "position": json.loads(i.inline_coordinates), "citance": i.citance}
                    for i in cited_papers
                ],
            }
        ),
        200,
    )


@app.route("/backend/store/survey/<string:token>", methods=["POST"])
@transactional
def store_info(db_session, token):
    """
    :type token: str
    """
    results = db_session.query(CitedPaper, CitingPaper).filter(
        CitingPaper.url_token == token,
        CitedPaper.id == request.json["id"],
        CitedPaper.completion_date.is_(None)
    ).one_or_none()

    if not results:
        return "", 200

    cited_paper, citing_paper = results
    cited_paper.completion_date = arrow.utcnow()
    citing_paper.completion_date = arrow.utcnow()

    if not cited_paper.citation_type:
        if "firstQuestion" in request.json:
            cited_paper.citation_type = CitationType.from_string(request.json["firstQuestion"])

    if not cited_paper.com_com_type:
        if "comComFirstQuestion" in request.json:
            cited_paper.com_com_type = ComComType.from_string(request.json["comComFirstQuestion"])

    if not cited_paper.citation_inf_inf_rank:
        if "secondQuestion" in request.json:
            cited_paper.citation_inf_inf_rank = InfRank.from_string(request.json["secondQuestion"])

    return "", 200
