# -*- coding: utf-8 -*-

import enum

from sqlalchemy import Enum, Column, String, Integer, ForeignKey, Float, Text, SmallInteger
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy_utils import ArrowType

Base = declarative_base()


@enum.unique
class CitationType(enum.Enum):
    BACKGROUND = 0
    USES = 1
    COMPARES_CONTRASTS = 2
    MOTIVATION = 3
    EXTENSION = 4
    FUTURE = 5

    @classmethod
    def from_string(cls, value):
        if value == "background":
            return cls.BACKGROUND
        elif value == "uses":
            return cls.USES
        elif value == "compares_contrasts":
            return cls.COMPARES_CONTRASTS
        elif value == "motivation":
            return cls.MOTIVATION
        elif value == "extension":
            return cls.EXTENSION
        elif value == "future":
            return cls.FUTURE
        else:
            return None


@enum.unique
class InfRank(enum.Enum):
    INCIDENTAL = 0
    INFLUENTIAL = 1

    @classmethod
    def from_string(cls, value):
        if value == "incidental":
            return cls.INCIDENTAL
        elif value == "influential":
            return cls.INFLUENTIAL
        else:
            return None


@enum.unique
class ComComType(enum.Enum):
    SIMILARITIES = 0
    REFUTING = 1
    DIFFERENCES = 2

    @classmethod
    def from_string(cls, value):
        if value == "similarities":
            return cls.SIMILARITIES
        elif value == "differences":
            return cls.DIFFERENCES
        elif value == "refuting":
            return cls.REFUTING
        else:
            return None


class CitedPaper(Base):
    __tablename__ = "cited_papers"

    id = Column(Integer, primary_key=True, autoincrement=True)
    ref_id = Column(String(100))
    title = Column(String(5000))
    author = Column(String(500))
    pub_year = Column(SmallInteger, nullable=True)
    citance = Column(String(5000))
    citance_rank = Column(Integer)
    bib_ref_coordinates = Column(Text)
    inline_coordinates = Column(Text)
    citation_type = Column(Enum(CitationType), nullable=True)
    com_com_type = Column(Enum(ComComType), nullable=True)
    citation_inf_inf_rank = Column(Enum(InfRank), nullable=True)
    citing_paper = relationship("CitingPaper", backref="cited_papers")
    completion_date = Column(ArrowType, nullable=True)
    url_token = Column(String(100), ForeignKey("citing_papers.url_token", ondelete="CASCADE"))

class CitingPaper(Base):
    __tablename__ = "citing_papers"

    id = Column(Integer, primary_key=True, autoincrement=True)
    ref_id = Column(String(100))
    mag_id = Column(Integer)
    core_id = Column(Integer)
    title = Column(String(5000))
    author = Column(String(500))
    pub_year = Column(SmallInteger, nullable=True)
    email = Column(String(500))
    page_height = Column(Float)
    page_width = Column(Float)
    url_token = Column(String(100), unique=True)
    completion_date = Column(ArrowType, nullable=True)
