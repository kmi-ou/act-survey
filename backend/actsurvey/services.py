# -*- coding: utf-8 -*-

from . import settings

import atexit
import logging
from functools import lru_cache, wraps
from multiprocessing import current_process
from threading import current_thread

import sqlalchemy
from sqlalchemy.orm import scoped_session, sessionmaker


services = {}


def service(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        global services
        process = current_process()
        key = (process.name, process.pid, f.__name__, tuple(args), tuple(sorted(kwargs.items())))
        initialized_service = services.get(key)
        if initialized_service is None:
            initialized_service = f(*args, **kwargs)
            services[key] = initialized_service
        return initialized_service

    return wrapper


@service
def database_connection():
    """
    :rtype: sqlalchemy.engine.base.Engine
    """
    return sqlalchemy.create_engine(settings.DATABASE_URL, pool_size=settings.DATABASE_POOL_SIZE, pool_recycle=1200)


def key_for_scoped_session():
    process, thread = current_process(), current_thread()
    return "%s-%s-%s" % (process.name, process.pid, thread.ident)


@service
def sql_session():
    """
    :rtype: sqlalchemy.orm.scoping.scoped_session
    """
    return scoped_session(sessionmaker(autocommit=False, autoflush=False, expire_on_commit=False, bind=database_connection()), scopefunc=key_for_scoped_session)


@atexit.register
def close_sql():
    """
    Close database session and connection.
    """
    _scoped_session = sql_session()
    _scoped_session.remove()

    process = current_process()
    key = (process.name, process.pid, database_connection.__name__, tuple(), tuple())
    initialized_database_connection = services.get(key)
    if initialized_database_connection:
        initialized_database_connection.dispose()


def logger(name="actsurvey"):
    """
    :type name: str
    :rtype: logging.Logger
    """
    return _build_logger(name)


@lru_cache(maxsize=None)
def _build_logger(name):
    """
    :type name: str
    :rtype: logging.Logger
    """
    logger_ = logging.getLogger(name)
    logger_.setLevel(settings.LOGGING_LEVEL)

    handler = logging.StreamHandler()
    handler.setLevel(settings.LOGGING_LEVEL)
    handler.setFormatter(logging.Formatter(settings.LOGGING_FORMAT))

    logger_.addHandler(handler)
    logger_.propagate = False

    return logger_
