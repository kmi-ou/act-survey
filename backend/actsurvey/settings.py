import logging

HTTP_API_PORT = 5000

LOGGING_FORMAT = "[%(levelname)s] %(asctime)-15s %(message)s"
LOGGING_LEVEL = logging.INFO

DATABASE_DIALECT = "mysql"
DATABASE_LOGIN = "sqladmin"
DATABASE_PASSWORD = "MzlhNTY4YzQ5ZTAz"
DATABASE_NAME = "actsurvey"
DATABASE_HOST = "127.0.0.1"
DATABASE_PORT = 3306
DATABASE_POOL_SIZE = 3

DATABASE_URL = "{dialect}://{login}:{password}@{host}:{port}/{name}".format(
    dialect=DATABASE_DIALECT, login=DATABASE_LOGIN, password=DATABASE_PASSWORD, host=DATABASE_HOST, port=DATABASE_PORT, name=DATABASE_NAME
)
