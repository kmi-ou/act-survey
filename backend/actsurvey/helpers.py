# -*- coding: utf-8 -*-

from functools import wraps

from . import services


def convert_to_inline_coordinates(raw_coordinates):
    """
    Convert raw inline_coordinates `6,178.90,173.28,15.92,11.30` into JSON structure
    :type raw_coordinates: str
    :rtype: dict
    """
    raw_coordinates = raw_coordinates.split(",")

    return {
        "page_number": int(raw_coordinates[0]),
        "coordinates": {
            "x1": float(raw_coordinates[1]) * 0.353,
            "y1": float(raw_coordinates[2]) * 0.353,
            "x2": float(raw_coordinates[1]) * 0.353 + float(raw_coordinates[3]) * 0.353,
            "y2": float(raw_coordinates[2]) * 0.353 + float(raw_coordinates[4]) * 0.353,
        },
    }


def convert_to_bib_ref_coordinates(raw_coordinates):
    """
    Convert raw bib_ref_coordinates
    `7,326.31,468.99,227.29,9.04;7,326.30,479.43,227.35,9.04;7,326.30,489.75,199.94,9.04` into JSON structure
    :type raw_coordinates: str
    :rtype: dict
    """
    raw_coordinates = [i.split(",") for i in raw_coordinates.split(";")]
    return [{"page_number": i[0], "coordinates": [{"x": i[1], "y": i[2]}, {"x": i[3], "y": i[4]}]} for i in raw_coordinates]


def transactional(func):
    """
    Do `func` within a transaction.
    """
    @wraps(func)
    def wrapper(*args, **kwargs):
        db_session = services.sql_session()
        logger = services.logger()
        try:
            result = func(db_session, *args, **kwargs)
            db_session.commit()
            return result
        except:
            db_session.rollback()
            logger.exception("Unexpected exception raised in transaction.")
            raise
    return wrapper
