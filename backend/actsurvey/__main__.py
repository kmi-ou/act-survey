# -*- coding: utf-8 -*-

"""
Usage:
    actsurvey import data [<citing_papers> <cited_papers>]
    actsurvey api [--port=<port>]
    actsurvey db-init
    actsurvey --help
"""

import os
import csv
import json
from docopt_dispatch import dispatch

from . import settings, services
from .api_server import app
from .database.db_model import Base, CitingPaper, CitedPaper
from .helpers import convert_to_inline_coordinates, convert_to_bib_ref_coordinates


@dispatch.on("api")
def cli_run_api_server(port, **kwargs):
    """
    This is rather for development usage. API on production should be run via Gunicorn
    :type port: None | str
    """
    app.run(port=settings.HTTP_API_PORT if port is None else int(port))


@dispatch.on("db-init")
def cli_db_init(**kwargs):
    print("This will drop & create all tables. To continue press 'c'")
    if input() != "c":
        return

    Base.metadata.drop_all(services.database_connection())
    Base.metadata.create_all(services.database_connection())


@dispatch.on("import", "data")
def cli_import_data(citing_papers, cited_papers, **kwargs):
    """
    Import data from input folder into database
    """
    db_session = services.sql_session()
    if not citing_papers:
        citing_papers = "input/new_citing_papers.tsv"

    if os.path.exists(citing_papers):
        with open(citing_papers) as citing_papers:
            citing_papers = csv.DictReader(citing_papers, delimiter="\t")
            # TODO: Do it in the bulks
            mapped_citing_papers = []
            for citing_paper in citing_papers:
                mapped_citing_papers.append(
                    dict(
                        ref_id=citing_paper["REF_ID"],
                        mag_id=citing_paper["Citing_MAG_ID"],
                        core_id=citing_paper["core_id"],
                        title=citing_paper["Citing Paper Title"],
                        pub_year=int(citing_paper["Citing Paper Pub Year"]) if citing_paper["Citing Paper Pub Year"].isdigit() else None,
                        author=citing_paper["Citing Author"],
                        email=citing_paper["Citing Author Email"],
                        url_token=citing_paper["url_token"],
                        page_height=citing_paper["page_height"],
                        page_width=citing_paper["page_width"],
                        completion_date=None,
                    )
                )

            db_session.bulk_insert_mappings(CitingPaper, mapped_citing_papers)
            db_session.commit()

    else:
        services.logger().error("Please provide correct file path for `citing_papers.tsv`")

    if not cited_papers:
        cited_papers = "input/new_cited_papers.tsv"

    if os.path.exists(cited_papers):
        with open(cited_papers) as cited_papers:
            cited_papers = csv.DictReader(cited_papers, delimiter="\t")
            # TODO: Do it in the bulks
            mapped_cited_papers = []
            for cited_paper in cited_papers:
                mapped_cited_papers.append(
                    dict(
                        ref_id=cited_paper["REF_ID"],
                        title=cited_paper["Cited Paper Title"],
                        pub_year=int(cited_paper["Cited paper Pub Year"]) if cited_paper["Cited paper Pub Year"].isdigit() else None,
                        author=cited_paper["Cited Paper Author"],
                        citance=cited_paper["Citance"],
                        citance_rank=int(cited_paper["Citance_rank"]),
                        bib_ref_coordinates=json.dumps(convert_to_bib_ref_coordinates(cited_paper["bib_ref_coordinates"])),
                        inline_coordinates=json.dumps(convert_to_inline_coordinates(cited_paper["inline_coordinates"])),
                        url_token=cited_paper["url_token"]
                    )
                )

            db_session.bulk_insert_mappings(CitedPaper, mapped_cited_papers)
            db_session.commit()

    else:
        services.logger().error("Please provide correct file path for `cited_papers.tsv`")


if __name__ == "__main__":
    dispatch(__doc__)
