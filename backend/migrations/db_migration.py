"""
RUN BEFORE SCRIPT:
    ALTER TABLE cited_papers ADD COLUMN url_token varchar(100);

RUN AFTER SCRIPT:
    ALTER TABLE cited_papers MODIFY url_token varchar(100) NOT NULL;
    ALTER TABLE cited_papers ADD FOREIGN KEY (url_token) REFERENCES citing_papers(url_token);
    ALTER TABLE cited_papers DROP FOREIGN KEY `cited_papers_ibfk_1`;
    ALTER TABLE citing_papers DROP INDEX ref_id;
"""
import os
import sys

# Start Ignoring PycodestyleBear
sys.path.append(os.getcwd())

from actsurvey.database.db_model import CitedPaper, CitingPaper, ComComType, CitationType, InfRank
from actsurvey import services


db_session = services.sql_session()
logger = services.logger()
try:
    citing_papers = db_session.query(CitingPaper).all()
    for citing_paper in citing_papers:
        cited_papers = (
            db_session.query(CitedPaper).filter(
                CitedPaper.ref_id == citing_paper.ref_id
            ).order_by(CitedPaper.citance_rank).all()
        )
        for cited_paper in cited_papers:
            cited_paper.url_token = citing_paper.url_token

    db_session.commit()
except:
    db_session.rollback()
    logger.exception("Unexpected exception raised in transaction.")
    raise
