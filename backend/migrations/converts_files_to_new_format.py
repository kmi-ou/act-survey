import csv
import copy

PATH_TO_CITING_PAPERS = "../input/ref_act_core_citing_papers.txt"
PATH_TO_CITED_PAPERS = "../input/ref_act_core_cited_papers.txt"

ref_id_to_url_token = {}
updated_cited_papers = []

with open(PATH_TO_CITING_PAPERS) as citing_papers_csv, open(PATH_TO_CITED_PAPERS) as cited_papers_csv:
    citing_papers_csv_reader = csv.DictReader(citing_papers_csv, delimiter="\t")
    for row in citing_papers_csv_reader:
        ref_id_to_url_token[row["REF_ID"]] = row["url_token"]

    cited_papers_csv_reader = csv.DictReader(cited_papers_csv, delimiter="\t")
    for row in cited_papers_csv_reader:
        row["url_token"] = ref_id_to_url_token[row["REF_ID"]]
        updated_cited_papers.append(row)

with open(PATH_TO_CITED_PAPERS, "w") as file:
    header_keys=updated_cited_papers[0].keys()
    writer = csv.DictWriter(file, fieldnames=updated_cited_papers[0].keys(), delimiter="\t")
    writer.writeheader()
    for row in updated_cited_papers:
        new_row = copy.deepcopy(row)
        for key in row:
            if key not in header_keys:
                del new_row[key]

        writer.writerow(new_row)
